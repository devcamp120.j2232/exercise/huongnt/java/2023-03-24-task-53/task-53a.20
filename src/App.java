import com.devcamp.task53.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        //khởi tạo đối tượng rectangle1 (k tham số)
        Rectangle rectangle1 = new Rectangle();
        //khởi tạo đối tượng rectangle2 (có tham số)
        Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());

        //lấy diện tích
        double dienTich1 = rectangle1.getArea();
        double dienTich2 = rectangle2.getArea();
        //lấy chu vi
        double chuVi1 = rectangle1.getPerimeter();
        double chuVi2 = rectangle2.getPerimeter();
        System.out.println("Diện tích của rectangle1 = " + dienTich1 + ", chu vi = " + chuVi1);
        System.out.println("Diện tích của rectangle2 = " + dienTich2 + ", chu vi = " + chuVi2);

    }
}
