package com.devcamp.task53;

public class Rectangle {
    // thuôc tính chiều dài
    private float length = 1.0f;
    // thuộc tính chiều rộng
    private float width = 1.0f;

    // phương thức khởi tạo không có tham số
    public Rectangle() {
        super();
    }

    /**
     * //phương thức khởi tạo có tham số
     * 
     * @param length
     * @param width
     */

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    /**
     * getter
     * 
     * @return
     */

    public float getLength() {
        return length;
    }

    /**
     * setter
     * 
     * @param length
     */

    /**
     * getter
     * 
     * @return
     */

    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    /**
     * setter
     * 
     * @param length
     */
    public void setWidth(float width) {
        this.width = width;
    }

    /**
     * //phương thức tính diện tích hình chữ nhật
     * 
     * @return
     */
    public double getArea() {
        return length * width;
    }

    /**
     * //phương thức tính chu vi
     * 
     * @return
     */
    public double getPerimeter(){
        return (length + width)*2;
    }

    /**
     * In ra console
    */
    @Override
    public String toString(){
        return String.format("Rectangle [length = %s, width = %s]", length, width);
    }

}
